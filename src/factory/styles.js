const pUtil = require('../utils/paths')
const fUtil = require('../utils/files')
const compilers = require('../compilers/index')
const R = require('ramda')
require('colors')

module.exports = {
    pathNodeModule: (path) => `../../${path}`,
    build(fileDefList = []){
        const filesToCompile =
            fileDefList
            .filter(el => !el.pathType.toLowerCase().includes('project'))
            .map(f => {
                return {
                    ...f,
                    compiler: f['compiler'].toLowerCase()
                }
            })

        const readyToCompile = filesToCompile.every(file => compilers[file.compiler])
        if (!readyToCompile){
            this.throwNotFoundCompilers(filesToCompile)
            return;
        }

        filesToCompile.map(async files => {
            await compilers[files.compiler](files)
        })
    },
    throwNotFoundCompilers(wrongFiles){
        const toRed = (str) => str.red
        wrongFiles.filter(file =>{
            return !Object.keys(compilers)
                .find(key => key.includes(file[`compiler`]))
        })
        .map(file => {
            throw `${toRed('ERROR')} compiler ${file.compiler} not found`
        })
    }
}