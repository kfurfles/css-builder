const { pipe, partial } = require('ramda')
const { pathType, compiler, customValidation } = require('./validations')
const { createFile } = require('./../../utils/files')
const { createFilePath } = require('./../../utils/paths')
require('colors')

const SCSS = {
    async build(fileDef){
        this.validation(fileDef)
        const { type } = fileDef

        return await this.types[type](fileDef)
    },
    createStyle(param,text){
        return pipe(
            createFile(
                createFilePath(param)
            )
        )(text)
    },
    createWithFolder([ folder, fileName ],text){
        return this.createStyle([folder, fileName], text)
    },
    getFileName(path){
        const allMatchs = path.match(/(\/[0-1a-z-A-Z]+)/gi)
        let name = allMatchs.slice(allMatchs.length -1)[0]
            .replace('/','')
            .replace('.scss','')
        
        return name
    },
    createImportRow: (path) => `@import "${path}";`,
    validation(fileDef){
        const { type } = fileDef
        const typeList = Object.keys(this.types)
        const validation = typeList.indexOf(type) > -1
        
        return pipe(
            pathType,
            compiler,
            partial(customValidation, [ validation, 'type', typeList ])
        )(fileDef)
    },
    buildFile(rows = []){
        return rows.join('\n')
    },
    types:{
        globals(fileDef){
            const { framework, required, insert } = fileDef

            const fileName = `${framework}.scss`
            const textFile = SCSS.buildFile([
                ...required.map(path => {
                    return SCSS.createImportRow(`../../${path}`)
                }),
                ...insert.map(path => {
                    return SCSS.createImportRow(`${framework}/${path}`)
                }),
            ])
            return SCSS.createStyle(fileName,textFile)
        },
        component(fileDef){
            const { path, framework } = fileDef
        
            const fileName = SCSS.getFileName(path)+'.scss'
            const textFile = SCSS.buildFile([
                SCSS.createImportRow(`../../../${path}`) 
            ])
            return SCSS.createWithFolder([framework,fileName],textFile)
        }
    }
}
    
module.exports = SCSS