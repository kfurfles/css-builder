const validationRules = {
    pathType(fileDef){
        const { pathType } = fileDef
        const response = !pathType || `module`.includes(pathType.toLowerCase())
        if(!response){
            throwError('pathType','module',fileDef)
        }
        return fileDef
    },
    compiler(fileDef){
        const { compiler } = fileDef
        const response = !compiler || `scss`.includes(compiler.toLowerCase())
        if(!response){
            throwError('compiler','scss',fileDef)
        }
        return fileDef
    },
    customValidation(cb,param,condition, fileDef){
        if (!cb) {
            throwError(param,condition, fileDef)
        } 
        return fileDef
    }
}

throwError = (param,condition,originalObject) =>{
    throw `${'ERROR'.red}: the param {${param}} must be "${condition}" file:\n\n${JSON.stringify(originalObject)}\n\n`
}

module.exports = validationRules