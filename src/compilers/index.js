module.exports = {
    scss: function(val){
        return require('./scss').build(val)
    }, 
    less: require('./less')().build
}