const { existsSync,
    unlink,
    mkdirSync,
    writeFile: wf } = require('fs')
const path = require('path')
const { promisify } = require('util')

const delFile = promisify(unlink)
const writeFile = promisify(wf)

module.exports = {
    createFile: (writePath) => (text) =>{
        try {
            const folderPath = path.dirname(writePath)
            if(existsSync(folderPath)){
                return writeFile(writePath,text)
            } else {
                mkdirSync(folderPath)
                return writeFile(writePath,text)
            }
        } catch (error) {
            throw error
        }
    },
    async deleteFile(path){
        try {
            return await delFile(path)
        } catch (error) {
            throw error
        }
    },
}
