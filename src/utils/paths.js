const { join, resolve } = require('path')
const FILES_PATH = join(__dirname,'..','files') 
module.exports = {
    createFilePath(paths) {
        if (Array.isArray(paths)) {
            const resolvedPath = join(FILES_PATH,...paths)
            return resolvedPath
        } else {
            const resolvedPath = resolve(FILES_PATH,paths)
            return resolvedPath
        }
    }
}